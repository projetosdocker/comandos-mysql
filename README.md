# Mecher com Mysql

#### Acessar Banco de dados Mysql localmente:
```
mysql -u USUARIO -p
```

#### Criar database:
```
CREATE DATABASE matomo_db_prod;
```

#### Criar usuário
```
CREATE USER 'matomoprod'@'localhost' IDENTIFIED WITH mysql_native_password BY 'SENHA-AQUI';
```

#### Garantir a permissão do usuário na database
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, INDEX, DROP, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON matomo_db_prod.* TO 'matomoprod'@'localhost';
